'''
The copyright of this dissertation rests with the author and no quotation from it or
information derived from it may be published without prior written consent of the
author.

This code can be run in the IDE of Quantopian.com

'''

import numpy as np
import pandas as pd
import datetime

from quantopian.algorithm import attach_pipeline, pipeline_output
from quantopian.pipeline import Pipeline, CustomFilter
from quantopian.pipeline.data.builtin import USEquityPricing
from quantopian.pipeline.factors import SimpleMovingAverage,CustomFactor
from quantopian.pipeline.data.quandl import fred_usdontd156n as libor
 

    
#=================================== GLOBAL VARIABLES ===================================#
LEVERAGE_MAX=1.0

SECTORS = [sid(19662),  # XLY Consumer Discrectionary  from SPDR Fund   
           sid(19656),  # XLF Financial                from SPDR Fund  
           sid(19658),  # XLK Technology               from SPDR Fund  
           sid(19655),  # XLE Energy                   from SPDR Fund 
           sid(19661),  # XLV Health Care              from SPDR Fund  
           sid(19657),  # XLI Industrial               from SPDR Fund  
           sid(19659),  # XLP Consumer Staples         from SPDR Fund
           sid(19654),  # XLB Materials                from SPDR Fund  
           sid(19660)]  # XLU Utilities                from SPDR Fund  

SAFE =  [sid(23921),    # TLT 20+  Year                T Bonds
         sid(23870)]    # IEF 7-10 Year                T Notes
      
    
     
#===================================  UNIVERSE ===================================#       
"""
The filters and factors help us restrict our universe and calculate the variables we want.
"""

class SIDInList(CustomFilter):
    """
    Checks if the asset is in a list (our trading universe).
    """    
    inputs = []
    window_length = 1
    params = ('sid_list',)

    def compute(self, today, assets, out, sid_list):
        out[:] = np.in1d(assets, sid_list)
        
class Sigma(CustomFactor):
    """
    Volatility in prices
    """
    inputs=[USEquityPricing.close]
    window_length=126
    
    def compute(self, today, asset_ids, out, values):
        # Calculates the column-wise standard deviation, ignoring NaNs
        out[:] = np.nanstd(values, axis=0)
     
    
class LiborFactor(CustomFactor): 
    """
    Average overnight libor over the last month
    """
    window_length = 21 
    inputs = [libor.value]

    def compute(self, today, assets, out, lib):  
        # Here vix might look like [[20]], but this will broadcast the same  
        # value into `out` for every asset.  
        out[:] = (lib.mean()/100)
       
    
class AvgReturn126(CustomFactor): 
    """
    Average return over the last 126 days
    """
    window_length = 126 
    inputs = [USEquityPricing.close]

    def compute(self, today, assets, out, close):  
        total_return = (close[-1] - close[0]) / close[0]
        avg_ret_126 = (total_return/126)
        out[:] = avg_ret_126
    
    
def make_pipeline():
    """
    The pipeline uses the above filters and facors to create the restricted universe. 
    It also associates desired variables to each security.
    """
    
    # Screen
    # In order: (TLT,IEF,XLY,XLF,XLK,XLE,XLV,XLI,XLP,XLB,XLU). Each sid corresponds to a security.
    in_universe= SIDInList(sid_list=(23921,23870,19662,19656,19658,19655,19661,19657,19659,19654,19660,8554))
    
    # Variables
    mean_close_126 = SimpleMovingAverage(inputs=[USEquityPricing.close], window_length=126)
    latest_close = USEquityPricing.close.latest
    sigma = Sigma()
    z_score = (latest_close-mean_close_126)/sigma
    Libor = LiborFactor()
    avg_return_126 = AvgReturn126()
    excess_return = (avg_return_126-Libor)
    
    
    return Pipeline(
        screen=in_universe,
        columns={
            '126d Mean Close': mean_close_126,
            'Latest Close': latest_close,
            'Sigma': sigma,
            'Z-Score': z_score,
            'Libor': Libor,
            'Avg R 126': avg_return_126,
            'Mu' : excess_return
        }
  )
 

    
#===================================  ALGO  ===================================#    
    
def initialize(context):
    """
    Called once at the start of the algorithm. 
        - Creates the above pipeline.
        - Organizes which function is called when.
        - Sets transaction and slippage costs
    """  
    # Set transaction costs 
    set_commission(commission.PerShare(cost=0.0075, min_trade_cost=1.00))
    
    # Set slippage costs
    set_slippage(slippage.VolumeShareSlippage(volume_limit=0.025, price_impact=0.1))
    
    # Execute trades at the beggining of each month
    schedule_function(my_rebalance,date_rules.month_start(),time_rules.market_open())
    
    # Plot leverage at the end of each day
    schedule_function(my_record_vars, date_rules.every_day(), time_rules.market_close())
     
    # Create our dynamic stock selector.
    my_pipe=make_pipeline()    
    attach_pipeline(my_pipe, 'my_pipeline')
    
   # Counts Rik on/off periods
    context.riskon=0
    context.riskoff=0
    
def before_trading_start(context, data):
    """
    Called every day before market open. 
        - Gets the updated pipeline results, on which we do the calculations.
    """
    context.output = pipeline_output('my_pipeline')

#===================================  WEIGHT FUNCTIONS  ===================================#   
def Risk_ON_weights(df,data):  
    """
    Calculate and appends tangency portfolio weights to our pipeline in risk-on periods
        - Mean Variance Analysis if more than one asset
        - 40% of portfolio on the sector if it is the only one we trade
    """
    # Securities
    securities=df.index
    
    # If we trade more than 1 security
    if len(securities) > 1:
        # Excess returns
        excess_returns=df['Mu']

        # Covariance matrix from daily returns
        returns_df=data.history(securities,'price',126,'1d').pct_change()[1:]
        returns_mat=returns_df.as_matrix().transpose()
        VCov=np.cov(returns_mat)
        inverse_VCov=np.linalg.inv(VCov)

        # Weights from formula
        numerator=np.dot(inverse_VCov,excess_returns)
        denominator=np.sum(np.dot(inverse_VCov,excess_returns))
        tangency_weights=numerator/denominator

        #We do never want to take on too much leverage, so we limit our position size
        leverage_tentative=np.absolute(tangency_weights).sum()
        adjustment=LEVERAGE_MAX/leverage_tentative
        tangency_weights_adjusted=tangency_weights*adjustment
        
        df['Weights'] = tangency_weights_adjusted

        return df

    # If only one security is traded.
    else:
        df['Weights']=[0.40]
        return df
    
def Risk_OFF_weigths(df,data):
    """
    Appends weights to our pipeline in risk-off periods
    """
    
    # Equal weights, half the portfolio value
    weights=np.array([0.375,0.375])
    df['Weights']=weights
    return df
 
    
#===================================  TRADING   ===================================# 
def my_rebalance(context,data):
    """
    Called every month. Does the actual trading.
    """
    
    exchange_time = pd.Timestamp(get_datetime()).tz_convert('US/Eastern')
    
    log.info('====================================== \n We trade at %s' % str(exchange_time))
    spy_z=context.output[context.output.index==sid(8554)]['Z-Score'].values # z score of the S&P 500
    
    
    # RISK ON
    if spy_z > -1.0 and spy_z < 1.0:
        log.info('RISK ON!')
        context.riskon+=1
        context.securities = context.output[(context.output['Z-Score'].values > -1.0) &
                                            (context.output['Z-Score'].values < 1.0) &  
                                            (context.output.index.isin(SECTORS))]
        context.output=Risk_ON_weights(context.securities,data)
        
        
        # Bonds
        for bond in SAFE:
            # First we make sure we do not already have open orders for that asset.
             if get_open_orders(bond): 
                continue 
             order_target_percent(bond,.00)
             log.info("Zero weight %s" % (bond.symbol))
        
        # Sectors
        for sect in SECTORS:
            
            # Sectors that we trade that month
            if sect in context.output.index:
                sect_weight=context.output[context.output.index==sect]['Weights'].values
                if get_open_orders(sect): 
                    continue
                order_target_percent(sect,sect_weight)
                log.info("Allocate %s" % (sect.symbol) + " with w= %s" % str(sect_weight))
            
            # Sectors that we do not trade that month
            else:
                if get_open_orders(sect): 
                    continue
                order_target_percent(sect,.00)
                log.info("Zero weight %s" % (sect.symbol))
        
        
    
    # RISK OFF
    else:
        log.info('RISK OFF...')
        context.riskoff+=1
        context.securities = context.output[context.output.index.isin(SAFE)]
        context.output=Risk_OFF_weigths(context.securities,data)
        
        # Sectors
        for sect in SECTORS:
            if get_open_orders(sect):
                continue
            order_target_percent(sect,.00)
            log.info("Zero weight %s" % (sect.symbol))
        
        # Bonds
        for bond in SAFE:
            bond_weight=context.output[context.output.index==bond]['Weights'].values
            if get_open_orders(bond): 
                continue 
            order_target_percent(bond,bond_weight)
            log.info("Allocate %s" % (bond.symbol) + " with w= %s" % str(bond_weight))
    
    # Log the gross leverage ratio
    log.info('Leverage: %s' %str(context.account.leverage))
    
    # Log how many risk on/off month there have been so far
    log.info('========>  risk on= %s' % (context.riskon))
    log.info('========>  risk off= %s' % (context.riskoff))
            

#===================================  PLOTTING  ===================================# 
 
def my_record_vars(context, data):
    """
    Saves variables for plotting.
    """
    # We plot the gross leverage ratio
    lev = context.account.leverage
    record(leverage = lev)
 
